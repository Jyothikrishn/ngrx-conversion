import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _baseURL: string = environment.baseURL;

  constructor(private _http: HttpClient) { }

  getPath(url: string) {
    return this._baseURL + url;
  }

  list<T>(url: string): Observable<T> {
    return this._http.get<T>(this.getPath(url));
  }

  listById<T>(url: string, id: string | number): Observable<T> {
    return this._http.get<T>(this.getPath(`${url}/${id}`))
  }

}
