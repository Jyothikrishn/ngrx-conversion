import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({

  imports: [
    BrowserModule,
    HttpClientModule
  ],
  exports: [
    BrowserModule,
    HttpClientModule
  ]
})
export class CoreModule { }
