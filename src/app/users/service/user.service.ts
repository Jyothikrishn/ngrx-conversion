import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { tap } from 'rxjs/operators'
import { ApiService } from 'src/app/shared/api.service';

import { User } from 'src/app/shared/model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly path = 'users'

  private _users: BehaviorSubject<Array<User>> = new BehaviorSubject<Array<User>>([]);

  constructor(private _api: ApiService) { }

  get users() {
    return this._users.asObservable();
  }

  setUsers(users: Array<User>) {
    this._users.next(users);
  }

  fetchUser(): Observable<Array<User>> {
    return this._api.list<Array<User>>(this.path).pipe(
      tap((users) => {
        if (users) {
          this.setUsers(users)
        }
      })
    )
  }

  fetchUserById(id: number): Observable<User> {
    return this._api.listById<User>(this.path, id);
  }
}
