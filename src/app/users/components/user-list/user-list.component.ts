import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  constructor(private _userService: UserService) { }

  get user() {
    return this._userService.users;
  }

  ngOnInit(): void {
    this._userService.fetchUser().subscribe(users => {
      console.log(users)
    })
  }

}
