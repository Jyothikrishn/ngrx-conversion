import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UserService } from '../../service/user.service';

import { User } from '../../../shared/model/user.model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  private _user!: Observable<User>;

  constructor(
    private _userService: UserService,
    private routes: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._user = this.routes.params.pipe(
      switchMap(param => {
        return this._userService.fetchUserById(param.id)
      })
    )
  }

  get user() {
    return this._user;
  }

}
